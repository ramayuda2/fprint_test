from django.contrib import admin
from .models import Produk, Kategori, Status
from .admin_views.ProdukAdminViews import ProdukAdmin


# Register your models here.


admin.site.register(Produk, ProdukAdmin)
admin.site.register(Kategori)
admin.site.register(Status)