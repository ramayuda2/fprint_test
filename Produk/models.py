from django.db import models

class Status(models.Model):
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = ('Status')
        verbose_name_plural = ('Status')

class Kategori(models.Model):
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = ('Kategori')
        verbose_name_plural = ('Kategori')


class Produk(models.Model):
    nama_produk = models.CharField(max_length=255)
    harga = models.IntegerField()
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
    staus = models.ForeignKey(Status, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_produk

    class Meta:
        verbose_name = ('Produk')
        verbose_name_plural = ('Produk')