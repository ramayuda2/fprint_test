from typing import Any
from django.contrib import admin
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from Produk.Utils import GetProdukApi


class ProdukAdmin(admin.ModelAdmin):
    list_display = ('nama_produk', 'harga', 'kategori', 'staus')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(staus__nama="bisa dijual")