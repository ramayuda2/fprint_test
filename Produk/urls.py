
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LogoutView

from Produk.views import GetDataApi

urlpatterns = [
    path("data-api/", GetDataApi, name="sync"),
]
