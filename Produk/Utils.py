import requests
import os
from .models import Kategori, Produk, Status
import datetime


def getDate():
    hari = datetime.datetime.now()
    return hari


def GetProdukApi(user, password):
    url = os.getenv('URL_API')

    payload = {
        'username': user,
        'password': password,
    }

    response = requests.post(url, data=payload)

    if response.status_code == 200:
        data = response.json()
        for i in data['data']:
            kategori, create_kategori = Kategori.objects.get_or_create(nama=i['kategori'])
            status, create_status = Status.objects.get_or_create(nama=i['status'])

            produk, created_produk = Produk.objects.get_or_create(
                nama_produk=i['nama_produk'],
                harga=int(i['harga']),
                kategori=kategori,
                staus=status
            )

        context = {
            'status': True,
            'data': response.json()
        }
        return context
    else:
        context = {
            'status': False,
            'data': response.text
        }
        return context

    