import json
import hashlib
from .Utils import GetProdukApi
from django.shortcuts import render

def GetDataApi(request):
    if request.method == 'POST':
        context = {
            'data': '',
            'status': False,
            'errors': ''
        }

        username = request.POST.get('username')
        password = hashlib.md5(request.POST.get('password').encode()).hexdigest()

        print(password)

        try:
            res = GetProdukApi(username, password)
            if res['status'] == False:
                context = {
                    'data': '',
                    'status': False,
                    'errors': res['data']
                }
            else :
                context = {
                    'data': res['data'],
                    'status': True,
                    'errors': ''
                }
        except Exception as e:
            context = {
                'data': '',
                'status': False,
                'errors': str(e)
            }
        return render(request, 'data_api.html', { 'context': context})
    
    return render(request, 'data_api.html', {})
    