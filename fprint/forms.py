# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))


class SignUpForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "placeholder": "Email",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))
    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password check",
                "class": "form-input ps-10 placeholder:text-white-dark"
            }
        ))
    
    is_superuser = forms.BooleanField(
         widget=forms.CheckboxInput(
            attrs={
                "placeholder": "Superuser",
                "class": "form-checkbox bg-white dark:bg-black"
            }
        )
    )

    is_staff = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                "placeholder": "Staff",
                "class": "form-checkbox bg-white dark:bg-black"
            }
        ))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'is_staff', 'is_superuser')
