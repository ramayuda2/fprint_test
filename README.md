# install Non Docker
- buat python enviroment dahulu didalam file project
    ```python3 -m virtualenv venv```
- masuk kedalam enviroment python
    ```source venv/bin/active ```
- install perlengkapan yang diperlukan dengan
    ``` pip install -r requirements.txt```
- lakukan make migrations unutk membuat model ke database sql
    ```python manage.py makemigrations```
- lakukan migrate setelah melakukan makemigrations (pastikasn django sudah tekoneksi ke database)
    ```python manage.py migrate```
- nyalakan server django 
    ```python manage.py runserver 0.0.0.0:8000```
- membuat superuser ( bisa lewat sign up web)
    ```python manage.py createsuperuser```
    - masukan data yang diperlukan oleh django
- jika ingin mengambil data dari api 
    - login kedalam admin
    - masuk submenu master data
    - masukan username API dan password API ( password tidak perlu di md5 karena sistem sudah ada convertnya ) masukan secara manual contoh: bisacoding-29-11-23
    - data akan tersync ketika memasukan username dan password dengan benar, jika ridak benar alert akan muncul dan menampilkan apa yang salah
 - jika ada kendala error seperti tailwind dan npm (install tailwind pada django dengan command berikut ini)
    ``` python manage.py tailwind install ```
    - jika masalah npm bisa melakukan custome NPM path pada setting.py
    - cari path npm pada laptop dengan command 
        ``` which npm ```
    - copy path yang tertera pastekan pada setting.py pada variable NPM_BIN_PATH

# install menggukan Docker
 - install docker di web
 - masuk kedalam folder project
    ``` docker-compose up```
    - otomatis docker akan menjalan kan penginstalan project yang diperlukan
    - jika belum mempunyai docker-compose bisa download terlebih dahulu
 - jika ingin makemigration dengan menggukan docker
    ``` docker-compose run web python manage.py makemigrations ```
 - jika ingin migrate dengan menggukan docker
    ``` docker-compose run web python manage.py migrate ```
 - jika ingin mengambil data dari api 
    - login kedalam admin
    - masuk submenu master data
    - masukan username API dan password API ( password tidak perlu di md5 karena sistem sudah ada convertnya ) masukan secara manual contoh: bisacoding-29-11-23
    - data akan tersync ketika memasukan username dan password dengan benar, jika ridak benar alert akan muncul dan menampilkan apa yang salah
 - jika ada kendala error seperti tailwind dan npm (install tailwind pada django dengan command berikut ini)
    ``` python manage.py tailwind install ```
    - jika masalah npm bisa melakukan custome NPM path pada setting.py
    - cari path npm pada laptop dengan command 
        ``` which npm ```
    - copy path yang tertera pastekan pada setting.py pada variable NPM_BIN_PATH
